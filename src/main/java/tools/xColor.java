package tools;

import java.awt.*;

public class xColor
{
    public static String colorToStr(Color c)
    {
        return String.format("%02X%02X%02X", c.getRed(), c.getGreen(), c.getBlue());
    }
}
