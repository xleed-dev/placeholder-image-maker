package tools;

public class Validator
{
    public static boolean validate(String s)
    {
        return s!=null && !s.trim().isEmpty();
    }

    public static boolean isNumber(String s)
    {
        try
        {
            Integer.parseInt(s);
            return true;
        }
        catch (NumberFormatException e)
        {
            return false;
        }
    }
}
