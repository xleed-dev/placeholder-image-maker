package ui;

import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.IntelliJTheme;
import tools.Validator;
import tools.xColor;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;

public class MainUI
{
    private JPanel mainPane;
    private JPanel dimensionsPane;
    private JPanel colorsPane;
    private JTextField widthTF;
    private JTextField heightTF;
    private JTextField backgroundTF;
    private JTextField foregroundTF;
    private JButton pickBackgroundBtn;
    private JButton pickForegroundBtn;
    private JTextField helloTextField;
    private JButton copyButton;
    private JComboBox<String> comboBox1;
    private JCheckBox previewCheckBox;
    private JPanel controlsPane;
    private JPanel previewPane;
    private JButton downloadButton;
    private JProgressBar progressBar1;
    private JLabel previewLabel;
    private JScrollPane scrollPane;

    private SwingWorker sw;
    private String currentURL;
    private boolean comboChangedInUI = false;
    private BufferedImage currentImg;
    private final int TIME_THRESHOLD = 1000;
    private final String[] RESOLUTIONS = { "500x300", "300x300",
            "400x600","600x400",
            "800x300", "1200x500",
            "1920x1080"
    };

    public MainUI()
    {
        DocumentListener defaultDocumentListener = new DocumentListener()
        {
            @Override public void insertUpdate(DocumentEvent e) {checkAndDownload();}
            @Override public void removeUpdate(DocumentEvent e) {checkAndDownload();}
            @Override public void changedUpdate(DocumentEvent e) {checkAndDownload();}
        };
        DocumentListener resDocumentListener = new DocumentListener()
        {
            @Override public void insertUpdate(DocumentEvent e) {checkCombo();}
            @Override public void removeUpdate(DocumentEvent e) {checkCombo();}
            @Override public void changedUpdate(DocumentEvent e) {checkCombo();}
            private void checkCombo()
            {
                if(!comboChangedInUI)
                {
                    comboBox1.setSelectedIndex(-1);
                    comboChangedInUI = false;
                }
            }
        };
        widthTF.getDocument().addDocumentListener(defaultDocumentListener);
        widthTF.getDocument().addDocumentListener(resDocumentListener);
        heightTF.getDocument().addDocumentListener(defaultDocumentListener);
        heightTF.getDocument().addDocumentListener(resDocumentListener);
        helloTextField.getDocument().addDocumentListener(defaultDocumentListener);
        backgroundTF.getDocument().addDocumentListener(defaultDocumentListener);
        foregroundTF.getDocument().addDocumentListener(defaultDocumentListener);
        previewCheckBox.addItemListener(e -> {
            if(e.getStateChange()== ItemEvent.SELECTED)  {checkAndDownload();}
            else {previewLabel.setIcon(null);previewLabel.setText("Preview disabled");}
        });

        comboBox1.addActionListener(e ->
        {
            String res = (String) comboBox1.getSelectedItem();
            if(!Validator.validate(res)) return;
            String[] split = res.split("[X|x]");
            comboChangedInUI = true;
            widthTF.setText(split[0]);
            heightTF.setText(split[1]);
            comboChangedInUI = false;
        });

        comboBox1.setRenderer((list, value, index, isSelected, cellHasFocus) ->
                new JLabel((index==-1 && value==null)?"Select a predefined resolution...":value,SwingConstants.CENTER));
        comboBox1.setSelectedIndex(-1);

        pickBackgroundBtn.addActionListener(e ->
        {
            Color nc = JColorChooser.showDialog(mainPane,"Background Color",
                    Color.decode("#"+backgroundTF.getText()));
            if(nc!=null) {backgroundTF.setText(xColor.colorToStr(nc));}
        });
        pickForegroundBtn.addActionListener(new ActionListener()
        {
            @Override public void actionPerformed(ActionEvent e)
            {
                Color nc = JColorChooser.showDialog(mainPane,"Foreground Color",
                        Color.decode("#"+foregroundTF.getText()));
                if(nc!=null)  { foregroundTF.setText(xColor.colorToStr(nc)); }
            }
        });

        copyButton.addActionListener(e ->
        {
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(new StringSelection(currentURL), null);
            final String oldText = copyButton.getText();
            copyButton.setText("Copied!");
            Timer t = new Timer(3000, e1 -> copyButton.setText(oldText));
            t.setRepeats(false);
            t.start();
        });

        downloadButton.addActionListener(e ->
        {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setAcceptAllFileFilterUsed(false);
            String file = helloTextField.getText().trim()
                    .replaceAll("[^a-zA-Z|\\s+]","")
                    .replaceAll("\\s+","_")
                    +".png";
            fileChooser.setSelectedFile(new File(file));
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PNG images", "png"));
            fileChooser.setCurrentDirectory(new File(Paths.get(System.getProperty("user.home"), "Desktop").toString()));
            final String oldText = downloadButton.getText();
            if(currentImg!=null && fileChooser.showSaveDialog(mainPane)==JFileChooser.APPROVE_OPTION)
            {
                try
                {
                    ImageIO.write(currentImg, "png", fileChooser.getSelectedFile());
                    downloadButton.setText("Saved!");
                    Timer t = new Timer(3000,(e1) -> downloadButton.setText(oldText));
                    t.setRepeats(false);
                    t.start();
                } catch (IOException ioException)
                {
                    ioException.printStackTrace();
                }
            }
        });

        comboBox1.setSelectedIndex(0);
        checkAndDownload();
    }

    private void checkAndDownload()
    {
        String width = widthTF.getText();
        if(!Validator.validate(width)) return;
        String height = heightTF.getText();
        if(!Validator.validate(height)) return;
        String text = helloTextField.getText().trim();
        if(!Validator.validate(text)) return;
        String background = backgroundTF.getText();
        if(!Validator.validate(background)) return;
        String foreground = foregroundTF.getText();
        if(!Validator.validate(foreground)) return;
        String url = String.format("https://via.placeholder.com/%sx%s/%s/%s?text=%s",
                width,height,background,foreground,text.replaceAll("\\s+","%20"));
        currentURL = url;
        if(previewCheckBox.isSelected()) downloadImgAndShowInIU(url);
    }

    private void downloadImgAndShowInIU(String url)
    {
        if(sw!=null) {sw.cancel(true);}
        sw = new SwingWorker()
        {
            ImageIcon icon;
            @Override protected Object doInBackground() throws Exception
            {
                SwingUtilities.invokeLater(() -> {
                    progressBar1.setIndeterminate(true);
                    previewLabel.setText("Downloading...");
                    previewLabel.setIcon(null);
                    downloadButton.setEnabled(false);
                });
                Thread.sleep(TIME_THRESHOLD);
                try
                {
                    currentImg = ImageIO.read(new URL(url));
                    icon = new ImageIcon(currentImg);
                    previewLabel.setText("");
                    previewLabel.setIcon(icon);
                    centerImage();
                } catch (IOException ioException)
                {
                    ioException.printStackTrace();
                    SwingUtilities.invokeLater(() -> {
                        previewLabel.setText(ioException.getMessage()
                                +". Check parameters and your internet");
                    });
                }
                return null;
            }

            @Override protected void done()
            {
                super.done();
                SwingUtilities.invokeLater(() -> {
                    downloadButton.setEnabled(true);
                    progressBar1.setIndeterminate(false);
                });
            }
        };
        sw.execute();
    }

    private void centerImage()
    {
        SwingUtilities.invokeLater(() -> {
            Rectangle bounds = scrollPane.getViewport().getViewRect();
            Dimension size = scrollPane.getViewport().getViewSize();
            int x = (size.width - bounds.width) / 2;
            int y = (size.height - bounds.height) / 2;
            scrollPane.getViewport().setViewPosition(new Point(x, y));
        });
    }

    public static void main(String[] args)
    {
        UIManager.put("ProgressBar.cycleTime", 1000);
        UIManager.put("ProgressBar.repaintInterval", 14);
        FlatLightLaf.install();
        IntelliJTheme.install( MainUI.class.getClassLoader()
                .getResourceAsStream("Atom One Light Contrast.theme.json" ) );
        JFrame f = new JFrame("PlaceholderMaker::Xleed");
        f.setPreferredSize(new Dimension(1200, 600));
        f.setMinimumSize(new Dimension(800,500));
        f.setContentPane(new MainUI().mainPane);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ImageIcon img = new ImageIcon(MainUI.class.getClassLoader().getResource("app-icon.png"));
        f.setIconImage(img.getImage());
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    private void createUIComponents()
    {
        comboBox1 = new JComboBox<>(RESOLUTIONS);
    }
}
